<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tentukan Nilai edited</title>
</head>

<body>
  <h1> Tugas Sanbercode </h1>

</body>
  <?php
    echo "<h3> soal no.1 </h3>";
    function tentukan_nilai($number)
    {
      if($number >= 85  && $number <= 100){
        echo "Sangat baik";
      }
      else if ($number >= 70 && $number < 85 ){
        echo "Baik";
      }
      else if($number >= 60 && $number < 70){
        echo "cukup";
      }
      else{
        echo "kurang";
      }
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    echo "(nilai 98) <br>";
    echo tentukan_nilai(76); //Baik
    echo "(nilai 76) <br>";
    echo tentukan_nilai(67); //Cukup
    echo "(nilai 67) <br>";
    echo tentukan_nilai(43); //Kurang
    echo "(nilai 43)";

    echo "<h3> Soal no.2 </h3>";
    function ubah_huruf($string){
      $abjad = "abcdefghijklmnopqrstuvwxyz";
      $output = "";
      for ($i = 0; $i < strlen($string); $i ++){
        $pos = strpos($abjad, $string[$i]);
        $output .= substr($abjad, $pos+1,1);
      }
      return $output. "<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    echo "<h3> Soal no.3 </h3>";
    function tukar_besar_kecil($string){
      $output = "";
      for ($j = 0; $j < strlen($string)-1; $j ++){
        if (ctype_upper($string[$j])){
          $output .= strtolower($string[$j]);
        }
        else if (ctype_lower($string[$j])){
          $output .= strtoupper($string[$j]);
        }
      }
      return $output."<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

  ?>
</html>
